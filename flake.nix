{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nobbz.url = "github:nobbz/nixos-config";

    oxalica.url = "github:oxalica/rust-overlay";

    cargo2nix.url = "github:cargo2nix/cargo2nix";
    cargo2nix.inputs.rust-overlay.follows = "oxalica";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    naersk,
    oxalica,
    nobbz,
    cargo2nix,
    ...
  }:
    flake-utils.lib.eachSystem ["x86_64-linux"] (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [oxalica.overlays.default cargo2nix.overlays.default];
      };

      rustVersion = pkgs.lib.pipe ./rust-toolchain [
        builtins.readFile
        builtins.fromTOML
        ({toolchain, ...}: toolchain.channel)
      ];

      rsPkgs = pkgs.rustBuilder.makePackageSet {
        inherit rustVersion;
        packageFun = import ./Cargo.nix;
      };

      rustTooling = let
        rust = pkgs.rust-bin.stable."${rustVersion}".default;
      in rec {
        inherit rust;
        rustc = rust;
        cargo = rust;
        rustPlatform = pkgs.makeRustPlatform {
          inherit cargo rustc;
        };
      };

      package = (builtins.fromTOML (builtins.readFile ./Cargo.toml)).package;
    in {
      formatter = pkgs.alejandra;

      packages.aoc = (rsPkgs.workspace.advent_of_code {}).bin;
      packages.default = self.packages.${system}.aoc;

      apps.test = {
        type = "app";
        program = "${pkgs.writeShellScript "test" "cargo test --release -- --format=terse -Z unstable-options --shuffle"}";
      };

      devShell = let
        # rust = (rustTooling.rust.override {extensions = ["rust-src" "rust-analysis" "rustfmt-preview" "rust-analyzer-preview"];})
        rust = rustTooling.rust.override {extensions = ["rust-src" "rust-analysis" "rustfmt-preview"];};
      in
        pkgs.mkShell {
          name = "${package.name}-dev-shell";
          inherit (package) version;

          packages = [
            cargo2nix.packages.x86_64-linux.default
            pkgs.cargo-outdated
            pkgs.nil
            pkgs.rust-analyzer
            pkgs.watchexec
            rust
          ];

          env.RUST_SRC_PATH = "${rust}/lib/rustlib/src/rust/library";
        };
    });
}
