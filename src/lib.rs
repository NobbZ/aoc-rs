// #![deny(clippy::all)]
// #![allow(clippy::redundant_field_names)]

use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

mod helper;
mod prelude;

/// Function used to hash answers.
fn hash<H>(val: H) -> u64
where
    H: Hash,
{
    let mut hasher = DefaultHasher::new();
    val.hash(&mut hasher);
    hasher.finish()
}

macro_rules! problem {
    (tests => [$($test:tt)*]; $($rest:tt)*) => {
        #[cfg(test)]
        mod problems {
            #![allow(unused)]
            use super::*;

            problem!(@test $($test)*);
        }

        #[allow(unused)]
        pub fn run_all(year: &str, day: &str, spoil: bool) {
            problem!(@print spoil year day $($test)*);
        }

        problem!($($rest)*);
    };


    (@bench $name:ident => {$test:expr, $exp:expr}, $($rest:tt)*) => {
        #[bench]
        fn $name(b: &mut Bencher) {
            b.iter(|| $test);
        }

        problem!(@bench $($rest)*);
    };

    (@bench) => {};

    (@test $name:ident => ($test:expr, $exp:expr), $($rest:tt)*) => {
        #[test]
        fn $name() {
            assert_eq!($test, $exp);
        }

        problem!(@test $(rest)*);
    };

    (@test $name:ident => {$test:expr, $exp:expr}, $($rest:tt)*) => {
        #[test]
        #[allow(clippy::unreadable_literal)]
        fn $name() {
            assert_eq!($crate::hash(&$test.unwrap()), $exp);
        }

        problem!(@test $($rest)*);
    };

    (@test) => {};

    (@print $spoil:ident $year:ident $day:ident $name:ident => ($test:expr, $exp:expr), $($rest:tt)*) => {
        println!("{:10} => {} = {:?}", stringify!($name), stringify!(&test), $test);
        problem!(@print $spoil $year $day $($rest)*)
    };

    (@print $spoil:ident $year:ident $day:ident $name:ident => {$test:expr, $exp:expr}, $($rest:tt)*) => {
        let start = ::std::time::Instant::now();
        let result = $test.unwrap();
        let duration = ::std::time::Instant::now().duration_since(start);

        if $spoil {
            println!(
                "{}::{}::{} <{:>20}> => {}",
                $year,
                $day,
                stringify!($name),
                $crate::hash(&result),
                result,
            );
        } else {
            println!(
                "{}::{}::{} <{:>20}>",
                $year,
                $day,
                stringify!($name),
                $crate::hash(&result),
            );
        }

        println!("{}::{}::{} {:?}", $year, $day, stringify!($name), duration);

        problem!(@print $spoil $year $day $($rest)*);
    };

    (@print $spoil:ident $year:ident $day:ident) => {};

    () => {};
}

macro_rules! load_input {
    ($year:tt $day:tt) => {
        include_str!(concat!(
            "../../../inputs/",
            stringify!($year),
            "/",
            stringify!($day),
            ".txt"
        ))
    };
}

macro_rules! modules {
    ($($year:ident => [$($day:ident,)*],)*) => {
        $(mod $year {
            $(pub mod $day;)*
        })*

        pub fn run_all(spoil: bool, _filters: &[&str]) {
            if spoil {
                println!("WARNING! SPOILERS ARE PRINTED!");
            }

            $($(
                self::$year::$day::run_all(stringify!($year), stringify!($day), spoil);
            )*)*
        }
    };
}

// TODO: re-add 2015/d17!
modules![
    y2015 => [d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18,],
    y2016 => [d01,],
    y2018 => [d01,],
    y2019 => [d01, d02, d03,],
    y2024 => [d01,],
];
