// #![deny(clippy::all)]
// #![deny(clippy::redundant_field_names)]

use std::env;

fn main() {
    let mut args = env::args();
    args.next();

    let mut spoil = false;
    let mut filters = Vec::new();

    for arg in args {
        match arg.as_str() {
            "--spoil" => spoil = true,
            arg => filters.push(arg.to_string()),
        }
    }

    let filters: Vec<&str> = filters.iter().map(|s| s.as_str()).collect();
    advent_of_code::run_all(spoil, &filters);
}
