use std::{
    collections::{BTreeSet, HashMap},
    i128,
};

use crate::prelude::*;

const INPUT: &str = load_input!(2024 01);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 11356407371831914975},
        part2 => {run_2(Cursor::new(INPUT)), 8575581338559188087},
    ];
}

fn run_1<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let (mut left, mut right) = r
        .lines()
        .map(|line| {
            line.unwrap()
                .split("   ")
                .map(|s| s.parse::<i128>().unwrap())
                .collect::<Vec<_>>()
        })
        .fold((Vec::new(), Vec::new()), |(mut l, mut r), xs| {
            l.push(xs[0]);
            r.push(xs[1]);

            (l, r)
        });

    left.sort();
    right.sort();

    Ok(left
        .into_iter()
        .zip(right.into_iter())
        .map(|(l, r)| (l - r).abs())
        .sum())
}

fn run_2<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let (mut left, mut right) = r
        .lines()
        .map(|line| {
            line.unwrap()
                .split("   ")
                .map(|s| s.parse::<i128>().unwrap())
                .collect::<Vec<_>>()
        })
        .fold((Vec::new(), Vec::new()), |(mut l, mut r), xs| {
            l.push(xs[0]);
            r.push(xs[1]);

            (l, r)
        });

    let frequencies = right.into_iter().fold(HashMap::new(), |mut fs, n| {
        fs.entry(n).and_modify(|count| *count += 1).or_insert(1);

        fs
    });

    Ok(left
        .into_iter()
        .filter_map(|n| frequencies.get(&n).map(|x| n * x))
        .sum())
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r#"3   4
4   3
2   5
1   3
3   9
3   3"#;

    #[test]
    fn part_1_example() {
        let c = Cursor::new(INPUT);

        let result = run_1(c).unwrap();

        assert_eq!(result, 11);
    }

    #[test]
    fn part_2_example() {
        let c = Cursor::new(INPUT);

        let result = run_2(c).unwrap();

        assert_eq!(result, 31);
    }
}
