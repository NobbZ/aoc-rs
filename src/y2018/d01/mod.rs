use std::collections::HashSet;

use crate::prelude::*;

const INPUT: &str = load_input!(2018 01);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 8400230153663738380},
        part2 => {run_2(Cursor::new(INPUT)), 3653195934333285574},
    ];
}

fn run_1<R>(r: R) -> Result<isize, Error>
where
    R: BufRead,
{
    Ok(r.lines()
        .map(|l| l.unwrap())
        .map(|s| s.parse::<isize>().unwrap())
        .sum())
}

fn run_2<R>(r: R) -> Result<isize, Error>
where
    R: BufRead,
{
    let mut seen = HashSet::new();
    let mut freq = 0;
    seen.insert(freq);

    let input: Vec<isize> = r
        .lines()
        .map(|l| l.unwrap())
        .map(|s| s.parse::<isize>().unwrap())
        .collect();

    for n in input.iter().cycle() {
        freq += n;

        if seen.contains(&freq) {
            return Ok(freq);
        }

        seen.insert(freq);
    }

    unreachable!("The loop above iterates infinitely.");
}

macro_rules! test_description {
    ($name:ident : $fun:ident, $input:expr => $exp:expr) => {
        #[test]
        fn $name() {
            let actual = $fun(Cursor::new($input));
            assert_eq!($exp, actual.unwrap());
        }
    };
}

test_description!(part_1_a: run_1, "+1\n-2\n+3\n+1" => 3);
test_description!(part_1_b: run_1, "+1\n+1\n+1" => 3);
test_description!(part_1_c: run_1, "+1\n+1\n-2" => 0);
test_description!(part_1_d: run_1, "-1\n-2\n-3" => -6);

test_description!(part_2_a: run_2, "+1\n-2\n+3\n+1" => 2);
test_description!(part_2_b: run_2, "+1\n-1" => 0);
test_description!(part_2_c: run_2, "+3\n+3\n+4\n-2\n-4" => 10);
test_description!(part_2_d: run_2, "-6\n+3\n+8\n+5\n-6" => 5);
test_description!(part_2_e: run_2, "+7\n+7\n-2\n-7\n-4" => 14);
