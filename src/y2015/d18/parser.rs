use super::light_grid::LightGrid;
use crate::prelude::*;

pub fn parse<R>(r: R) -> LightGrid
where
    R: BufRead,
{
    r.bytes()
        .filter_map(|b| match b {
            Ok(b'#') => Some(true),
            Ok(b'.') => Some(false),
            Ok(b'\n') => None,
            Ok(c) => unreachable!("Expected {:?}, got {:?}.", vec!['#', '.', '\n'], c),
            Err(e) => unreachable!("An error occured: {:?}", e),
        })
        .collect()
}
