use std::{cmp, iter::FromIterator};

use bitvec::vec::BitVec;
use integer_sqrt::IntegerSquareRoot;

pub struct LightGrid {
    vec: BitVec,
    dim: isize,
}

impl LightGrid {
    pub fn apply_override<F>(&mut self, o: F)
    where
        F: Fn(&Self, isize, isize) -> Option<bool>,
    {
        for x in 0..self.dim {
            for y in 0..self.dim {
                if let Some(v) = o(self, x, y) {
                    self.vec.set((x * self.dim + y) as usize, v)
                }
            }
        }
    }

    pub fn iterate<F>(&mut self, o: F)
    where
        F: Fn(&Self, isize, isize) -> Option<bool>,
    {
        let size = (self.dim * self.dim) as usize;
        let mut new = BitVec::with_capacity(size);

        for _ in 0..size {
            new.push(false)
        }

        for x in 0..self.dim {
            for y in 0..self.dim {
                let idx = (x * self.dim + y) as usize;

                // println!("{}, {}; {}", x, y, idx);

                if let Some(v) = o(self, x, y) {
                    new.set(idx, v)
                } else {
                    let neighbours = self.get_alive_neighbours(x, y);
                    let light = self.vec[idx];

                    if light && neighbours == 2 || neighbours == 3 {
                        new.set(idx, true);
                    } else {
                        new.set(idx, false);
                    }
                }
            }
        }

        self.vec = new;
    }

    pub fn corners(&self, x: isize, y: isize) -> Option<bool> {
        match (x, y) {
            (0, 0) => Some(true),
            (0, y) if y == self.dim - 1 => Some(true),
            (x, 0) if x == self.dim - 1 => Some(true),
            (x, y) if x == self.dim - 1 && y == self.dim - 1 => Some(true),
            _ => None,
        }
    }

    pub fn count_active(&self) -> usize {
        self.vec.count_ones()
    }

    pub fn get_alive_neighbours(&self, x: isize, y: isize) -> usize {
        [
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        ]
        .iter()
        .filter_map(|(dx, dy)| {
            let (new_x, new_y) = (x + dx, y + dy);

            if cmp::min(new_x, new_y) < 0 || new_x >= self.dim || new_y >= self.dim {
                None
            } else {
                Some(new_x * self.dim + new_y)
            }
        })
        .filter(|&x| self.vec[x as usize])
        .count()
    }
}

impl FromIterator<bool> for LightGrid {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = bool>,
    {
        let it = iter.into_iter();
        let mut vec = BitVec::with_capacity(it.size_hint().0);

        for b in it {
            vec.push(b);
        }

        let n = vec.len().integer_sqrt() as isize;

        LightGrid { vec, dim: n }
    }
}
