mod light_grid;
mod parser;

use self::light_grid::LightGrid;
use crate::prelude::*;

const INPUT: &str = load_input!(2015 18);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), 100, |_, _, _| None), 17922878356929717082},
        part2 => {run(Cursor::new(INPUT), 100, LightGrid::corners), 14114750189009003637},
    ];
}

fn run<R, F>(r: R, iterations: isize, override_fn: F) -> Result<usize, Error>
where
    R: BufRead,
    F: Fn(&LightGrid, isize, isize) -> Option<bool>,
{
    let mut lights = parser::parse(r);

    lights.apply_override(&override_fn);

    for _ in 0..iterations {
        lights.iterate(&override_fn);
    }

    Ok(lights.count_active())
}

#[cfg(test)]
const SAMPLE: &str = r#".#.#.#
...##.
#....#
..#...
#.#..#
####.."#;

#[test]
fn mini_example_part_1() {
    let actual = run(Cursor::new(SAMPLE), 4, |_, _, _| None).unwrap();

    assert_eq!(4, actual);
}

#[test]
fn mini_example_part_2() {
    let actual = run(Cursor::new(SAMPLE), 5, LightGrid::corners).unwrap();

    assert_eq!(17, actual);
}
