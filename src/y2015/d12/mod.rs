use serde_json::{error::Error, Map, Value};

use crate::prelude::*;

const INPUT: &str = load_input!(2015 12);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), |_| false), 17689439436018354838},
        part2 => {run(Cursor::new(INPUT), has_red), 10722877710393962987},
    ];
}

fn run<R, F>(r: R, filter: F) -> Result<i64, Error>
where
    R: Read,
    F: Fn(&Map<String, Value>) -> bool,
{
    let parsed: Value = serde_json::from_reader(r)?;

    Ok(sum(&parsed, &filter))
}

fn sum<F>(obj: &Value, filter: &F) -> i64
where
    F: Fn(&Map<String, Value>) -> bool,
{
    use serde_json::Value::*;

    match obj {
        Null => 0,
        Bool(_) => 0,
        Number(n) => n.as_i64().unwrap(),
        String(_) => 0,
        Array(arr) => arr.iter().map(|o| sum(o, filter)).sum(),
        Object(map) => {
            if filter(map) {
                0
            } else {
                map.values().map(|o| sum(o, filter)).sum()
            }
        }
    }
}

fn has_red(obj: &Map<String, Value>) -> bool {
    obj.values().any(|v| v == "red")
}
