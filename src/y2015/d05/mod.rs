use std::collections::HashMap;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 05);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), part_1), 18279137173405083757},
        part2 => {run(Cursor::new(INPUT), part_2), 4100106362216887809},
    ];
}

fn run<R, P>(r: R, p: P) -> Result<usize, Error>
where
    R: BufRead,
    P: Fn(&str) -> bool,
{
    let parsed = r.lines().map(|l| l.unwrap());
    Ok(parsed.filter(|s| p(s)).count())
}

fn part_1(s: &str) -> bool {
    let mut vowels = 0;
    let mut double = false;
    let mut badsub = false;

    let mut last = ' ';

    for c in s.chars() {
        if c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' {
            vowels += 1;
        };

        double = double || c == last;

        badsub = badsub
            || (last == 'a' && c == 'b')
            || (last == 'c' && c == 'd')
            || (last == 'p' && c == 'q')
            || (last == 'x' && c == 'y');

        if badsub {
            return false;
        }

        last = c;
    }

    vowels >= 3 && double
}

fn part_2(s: &str) -> bool {
    let mut pairs = HashMap::new();
    let mut last: Option<char> = None;
    let mut before_last: Option<char> = None;

    let mut gapped = false;

    for (idx, c) in s.chars().enumerate() {
        if let Some(l) = last {
            let pair = format!("{}{}", l, c);

            let positions = pairs.entry(pair).or_insert_with(Vec::new);
            positions.push(idx);
        }

        if let Some(bl) = before_last {
            gapped = gapped || bl == c;
        }

        before_last = last;
        last = Some(c);
    }

    let mut pair_found = false;

    for pos in pairs.values() {
        if pos.len() == 2 {
            pair_found = pos[1] - pos[0] >= 2;
        } else if pos.len() >= 3 {
            pair_found = true;
        }

        if pair_found {
            break;
        }
    }

    pair_found && gapped
}
