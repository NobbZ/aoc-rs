use crypto::{digest::Digest, md5::Md5};

pub struct Hash([u8; 16]);

impl Hash {
    pub fn new(hasher: &mut Md5, key: &[u8], idx: u64) -> Self {
        hasher.input(key);
        hasher.input(idx.to_string().as_bytes());

        let mut out = [0; 16];
        hasher.result(&mut out);

        hasher.reset();

        Hash(out)
    }

    pub fn leading_nibbles_are_zero(&self, n: usize) -> bool {
        (0..n).all(|i| self.nibble(i) == 0)
    }

    fn nibble(&self, n: usize) -> u8 {
        let b = self.0[n / 2];
        if n % 2 == 0 {
            (0xF0 & b) >> 4
        } else {
            0x0F & b
        }
    }
}

#[test]
fn nibble_1() {
    assert_eq!(1, Hash([0x12; 16]).nibble(0))
}

#[test]
fn nibble_2() {
    assert_eq!(2, Hash([0x12; 16]).nibble(1))
}

#[test]
fn leading_1() {
    assert!(Hash([0x02; 16]).leading_nibbles_are_zero(1))
}
