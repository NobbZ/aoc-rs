use crypto::md5::Md5;

const INPUT: &str = "yzbqklnj";

problem!{
    tests => [
        part1 => {run(INPUT, 5), 17050050772328613578},
        part2 => {run(INPUT, 6), 4686411364568213714},
    ];
}

mod hash;

fn run<S>(input: S, prefix: usize) -> Option<u64>
where
    S: AsRef<str>,
{
    let mut hasher = Md5::new();

    let key = input.as_ref().as_bytes();

    Some(
        (0..std::u64::MAX)
            .map(|i| (i, hash::Hash::new(&mut hasher, key, i)))
            .filter(|(_, hash)| hash.leading_nibbles_are_zero(prefix))
            .take(1)
            .collect::<Vec<_>>()[0]
            .0,
    )
}
