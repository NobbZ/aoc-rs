use std::{
    cmp,
    ops::{Add, Mul},
};

#[derive(Debug, PartialEq, Default, Clone)]
pub struct Ingredient {
    pub name: String,
    pub capacity: isize,
    pub durability: isize,
    pub flavor: isize,
    pub texture: isize,
    pub calories: isize,
}

impl Ingredient {
    pub fn score(&self) -> isize {
        self.capacity * self.durability * self.flavor * self.texture
    }

    pub fn clamp(&self) -> Ingredient {
        Ingredient {
            capacity: cmp::max(0, self.capacity),
            durability: cmp::max(0, self.durability),
            flavor: cmp::max(0, self.flavor),
            texture: cmp::max(0, self.texture),
            ..self.clone()
        }
    }
}

impl Add<Ingredient> for Ingredient {
    type Output = Ingredient;

    fn add(self, rhs: Ingredient) -> Self::Output {
        Ingredient {
            capacity: self.capacity + rhs.capacity,
            durability: self.durability + rhs.durability,
            flavor: self.flavor + rhs.flavor,
            texture: self.texture + rhs.texture,
            calories: self.calories + rhs.calories,
            ..Default::default()
        }
    }
}

impl Mul<isize> for &Ingredient {
    type Output = Ingredient;

    fn mul(self, rhs: isize) -> Self::Output {
        Ingredient {
            name: self.name.clone(),
            capacity: rhs * self.capacity,
            durability: rhs * self.durability,
            flavor: rhs * self.flavor,
            texture: rhs * self.texture,
            calories: rhs * self.calories,
        }
    }
}
