use lazy_static::lazy_static;
use regex::Regex;

use super::ingredient::Ingredient;
use crate::{helper::error, prelude::*};

pub fn parse<R>(r: R) -> impl Iterator<Item = Ingredient>
where
    R: BufRead,
{
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?P<name>[[:alpha:]]+): capacity (?P<cap>-?\d+), durability (?P<dur>-?\d+), flavor (?P<fla>-?\d+), texture (?P<tex>-?\d+), calories (?P<cal>-?\d+)").unwrap();
    }

    r.lines().map(|l| l.unwrap()).map(|l| {
        let caps = RE.captures(&l);

        match caps {
            Some(caps) => Ingredient {
                name: error::option_to_result(caps.name("name"))
                    .unwrap()
                    .as_str()
                    .to_string(),
                capacity: error::option_to_result(caps.name("cap"))
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                durability: error::option_to_result(caps.name("dur"))
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                flavor: error::option_to_result(caps.name("fla"))
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                texture: error::option_to_result(caps.name("tex"))
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                calories: error::option_to_result(caps.name("cal"))
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
            },
            None => unreachable!("Regex did not match in {:?}", l),
        }
    })
}
