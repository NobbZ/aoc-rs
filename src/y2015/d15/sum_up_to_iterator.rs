#[derive(Debug)]
pub struct SumUpToIterator {
    target_sum: isize,
    elements: usize,
    helper: Vec<isize>,
}

impl SumUpToIterator {
    pub fn new(target_sum: isize, elements: usize) -> Self {
        let helper = vec![0; elements];

        SumUpToIterator {
            target_sum,
            elements,
            helper,
        }
    }
}

impl Iterator for SumUpToIterator {
    type Item = Vec<isize>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut i = 0;

        while i < self.elements {
            if self.helper[i] < self.target_sum {
                self.helper[i] += 1;
                i = 0;
            } else {
                self.helper[i] = 0;
                i += 1;
                continue;
            }

            if self.helper.iter().sum::<isize>() == self.target_sum {
                return Some(self.helper.clone());
            }
        }

        None
    }
}
