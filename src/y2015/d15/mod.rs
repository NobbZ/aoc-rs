mod ingredient;
mod parser;
mod sum_up_to_iterator;

use std::cmp;

use self::{ingredient::Ingredient, sum_up_to_iterator::SumUpToIterator};
use crate::prelude::*;

const INPUT: &str = load_input!(2015 15);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 14561775443845480054},
        part2 => {run_2(Cursor::new(INPUT)), 3928535310615681264},
    ];
}

fn run_1<R>(r: R) -> Result<isize, Error>
where
    R: BufRead,
{
    let parsed: Vec<_> = parser::parse(r).collect();

    match SumUpToIterator::new(100, parsed.len())
        .map(|w| weight_ingredients(&w, &parsed))
        .map(|i| i.clamp())
        .map(|v| v.score())
        .max()
    {
        Some(max) => Ok(max),
        None => Err(format_err!("There was no input")),
    }
}

fn run_2<R>(r: R) -> Result<isize, Error>
where
    R: BufRead,
{
    let parsed: Vec<_> = parser::parse(r).collect();

    match SumUpToIterator::new(100, parsed.len())
        .map(|w| weight_ingredients(&w, &parsed))
        .map(|i| Ingredient {
            capacity: cmp::max(0, i.capacity),
            durability: cmp::max(0, i.durability),
            flavor: cmp::max(0, i.flavor),
            texture: cmp::max(0, i.texture),
            ..i
        })
        .filter(|i| i.calories == 500)
        .map(|i| i.score())
        .max()
    {
        Some(max) => Ok(max),
        None => Err(format_err!("There was no input")),
    }
}

fn weight_ingredients(weights: &[isize], ingredients: &[Ingredient]) -> Ingredient {
    weights
        .iter()
        .zip(ingredients.iter())
        .map(|(amnt, ingredient)| ingredient * *amnt)
        .fold(Default::default(), |acc: Ingredient, ing| acc + ing)
}

#[cfg(test)]
const EXAMPLE: &str = r#"Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"#;

#[test]
fn parse_test() {
    let parsed: Vec<_> = parser::parse(Cursor::new(EXAMPLE)).collect();

    assert_eq!(
        vec![
            Ingredient {
                name: "Butterscotch".to_string(),
                capacity: -1,
                durability: -2,
                flavor: 6,
                texture: 3,
                calories: 8
            },
            Ingredient {
                name: "Cinnamon".to_string(),
                capacity: 2,
                durability: 3,
                flavor: -2,
                texture: -1,
                calories: 3
            }
        ],
        parsed
    );
}

#[test]
fn run1_test() {
    let result = run_1(Cursor::new(EXAMPLE));

    assert_eq!(62_842_880, result.unwrap());
}

#[test]
fn run2_test() {
    let result = run_2(Cursor::new(EXAMPLE));

    assert_eq!(57_600_000, result.unwrap());
}
