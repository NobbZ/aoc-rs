use std::collections::HashSet;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 03);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), 1), 18185182352255752540},
        part2 => {run(Cursor::new(INPUT), 2), 8506448260470797010},
    ];
}

mod direction;
mod parser;
mod pos;

fn run<R>(reader: R, num_santas: usize) -> Result<usize, Error>
where
    R: Read,
{
    let center = pos::Pos { x: 0, y: 0 };
    let mut santas = Vec::with_capacity(num_santas);
    let mut houses = HashSet::new();
    houses.insert(center);

    (0..num_santas).for_each(|_| santas.push(center));

    parser::parse(reader).enumerate().for_each(|(idx, dir)| {
        let i = idx % num_santas;
        santas[i] = santas[i] + dir;
        houses.insert(santas[i]);
    });

    Ok(houses.len())
}

macro_rules! test_description {
    ($name:ident : $num:expr, $input:expr => $exp:expr) => {
        #[test]
        fn $name() {
            let result = run(Cursor::new($input), $num);
            assert_eq!($exp, result.unwrap());
        }
    };
}

// Part 1 samples
test_description!(r_delivers_to_2:          1, ">"          => 2);
test_description!(urdl_delivers_to_4:       1, "^>v<"       => 4);
test_description!(ududududud_delivers_to_2: 1, "^v^v^v^v^v" => 2);

// Part 2 samples
test_description!(ud_delivers_to_3_with_2_santas:          2, "^v"         =>  3);
test_description!(urdl_delivers_to_3_with_2_santas:        2, "^>v<"       =>  3);
test_description!(ududududud_delivers_to_11_with_2_santas: 2, "^v^v^v^v^v" => 11);
