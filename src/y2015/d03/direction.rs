#[derive(Debug)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}
