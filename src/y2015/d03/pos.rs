use std::ops::Add;

use super::direction::Direction;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub struct Pos {
    pub x: isize,
    pub y: isize,
}

impl Add<Direction> for Pos {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn add(self, rhs: Direction) -> Self::Output {
        match rhs {
            Direction::Up => Pos {
                x: self.x + 1,
                ..self
            },
            Direction::Right => Pos {
                y: self.y + 1,
                ..self
            },
            Direction::Down => Pos {
                x: self.x - 1,
                ..self
            },
            Direction::Left => Pos {
                y: self.y - 1,
                ..self
            },
        }
    }
}
