use super::direction::Direction;
use crate::prelude::*;

pub fn parse<R>(reader: R) -> impl Iterator<Item = Direction>
where
    R: Read,
{
    reader.bytes().filter_map(|b| match b.unwrap() as char {
        '^' => Some(Direction::Up),
        '>' => Some(Direction::Right),
        'v' => Some(Direction::Down),
        '<' => Some(Direction::Left),
        '\n' => None,
        c => unreachable!(
            "Expected {:?}, got {:?}.",
            vec!['^', '>', 'v', '<', '\n'],
            c
        ),
    })
}
