pub enum Paren {
    Open,
    Close,
}

impl From<Paren> for isize {
    fn from(value: Paren) -> Self {
        match value {
            Paren::Open => 1,
            Paren::Close => -1,
        }
    }
}
