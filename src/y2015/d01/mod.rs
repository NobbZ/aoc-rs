use crate::prelude::*;

const INPUT: &str = load_input!(2015 01);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 15803403730818557393},
        part2 => {run_2(Cursor::new(INPUT)), 1194529909011081947},
    ];
}

mod paren;
mod parser;

fn run_1<R>(r: R) -> Result<isize, Error>
where
    R: Read,
{
    parser::parse(r).map(|p| Ok(Into::<isize>::into(p))).sum()
}

fn run_2<R>(r: R) -> Result<usize, Error>
where
    R: Read,
{
    let pre_result = parser::parse(r)
        .map(Into::<isize>::into)
        .scan(0, |s, p| {
            *s += p;
            Some(*s)
        })
        .take_while(|i| *i >= 0)
        .count();

    Ok(pre_result + 1) // We need `+1` here to take the first basement into account we dropped above.
}

macro_rules! test_description {
    ($name:ident : $fun:ident, $exp:expr, $input:expr) => {
        #[test]
        fn $name() {
            let actual = $fun(Cursor::new($input));
            assert_eq!($exp, actual.unwrap());
        }
    };
}

// Part 1 samples
test_description!(part1_oocc_is_0: run_1, 0, "(())");
test_description!(part1_ococ_is_0: run_1, 0, "()()");
test_description!(part1_ooo_is_3: run_1, 3, "(((");
test_description!(part1_oocooco_is_3: run_1, 3, "(()(()(");
test_description!(part1_ccooooo_is_3: run_1, 3, "))(((((");
test_description!(part1_occ_is_m1: run_1, -1, "())");
test_description!(part1_cco_is_m1: run_1, -1, "))(");
test_description!(part1_ccc_is_m3: run_1, -3, ")))");
test_description!(part1_coccocc_is_m3: run_1, -3, ")())())");

// Part 2 samples
test_description!(part_c_is_1: run_2, 1, ")");
test_description!(part_ococc_is_5: run_2, 5, "()())");
