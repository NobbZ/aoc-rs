use super::paren::Paren;
use crate::prelude::*;

pub fn parse<R>(reader: R) -> impl Iterator<Item = Paren>
where
    R: Read,
{
    reader.bytes().filter_map(|b| match b.unwrap().into() {
        '(' => Some(Paren::Open),
        ')' => Some(Paren::Close),
        '\n' => None,
        c => unreachable!("Expected {:?}, got {:?}.", vec!['(', ')', '\n'], c),
    })
}
