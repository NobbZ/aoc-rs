use bitset::BitSet;

use super::light_grid::LightGrid;

pub struct LightSet(BitSet);

impl LightSet {
    pub fn new() -> Self {
        LightSet(BitSet::with_capacity(1000 * 1000))
    }
}

impl LightGrid for LightSet {
    fn turn_on(&mut self, coord: (usize, usize)) {
        self.0.set(coord.0 * 1000 + coord.1, true);
    }

    fn turn_off(&mut self, coord: (usize, usize)) {
        self.0.set(coord.0 * 1000 + coord.1, false);
    }

    fn toggle(&mut self, coord: (usize, usize)) {
        self.0.flip(coord.0 * 1000 + coord.1);
    }

    fn brightness(&self) -> usize {
        self.0.count() as usize
    }
}
