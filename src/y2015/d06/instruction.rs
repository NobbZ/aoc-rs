use std::str::FromStr;

use super::rectangle::Rectangle;
use crate::prelude::*;

const TOGGLE: &str = "toggle ";
const TURN_ON: &str = "turn on ";
const TURN_OFF: &str = "turn off ";

#[derive(Debug)]
pub enum Instruction {
    TurnOn(Rectangle),
    TurnOff(Rectangle),
    Toggle(Rectangle),
}

impl FromStr for Instruction {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use self::Instruction::*;

        if let Some(stripped) = s.strip_prefix(TOGGLE) {
            Ok(Toggle(stripped.to_string().parse()?))
        } else if let Some(stripped) = s.strip_prefix(TURN_ON) {
            Ok(TurnOn(stripped.to_string().parse()?))
        } else if let Some(stripped) = s.strip_prefix(TURN_OFF) {
            Ok(TurnOff(stripped.to_string().parse()?))
        } else {
            Err(format_err!(
                "Expected a line beginning with one of {:?}, got {:?}",
                vec![TOGGLE, TURN_ON, TURN_OFF],
                s,
            ))
        }
    }
}
