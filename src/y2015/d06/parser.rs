use super::instruction::Instruction;
use crate::prelude::*;

pub fn parse<R>(r: R) -> impl Iterator<Item = Instruction>
where
    R: BufRead,
{
    r.lines().map(|line| line.unwrap().parse().unwrap())
}
