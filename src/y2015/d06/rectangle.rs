use std::str::FromStr;

use crate::prelude::*;

#[derive(Debug)]
pub struct Rectangle {
    a: (usize, usize),
    b: (usize, usize),
}

impl Rectangle {
    pub fn covered_fields(&self) -> Fields {
        Fields {
            bx: self,
            x: self.a.0,
            y: self.a.1,
        }
    }
}

impl FromStr for Rectangle {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split = s.split(" through ").collect::<Vec<_>>();

        let start = split[0].split(',').collect::<Vec<_>>();
        let start = match start.as_slice() {
            [x, y] => (x.parse()?, y.parse()?),
            _ => return Err(format_err!("Can't parse Rectangle from {:?}", s)),
        };

        let end = split[1].split(',').collect::<Vec<_>>();
        let end = match end.as_slice() {
            [x, y] => (x.parse()?, y.parse()?),
            _ => return Err(format_err!("Can't parse Rectangle from {:?}", s)),
        };

        Ok(Rectangle { a: start, b: end })
    }
}

#[derive(Debug)]
pub struct Fields<'a> {
    bx: &'a Rectangle,
    x: usize,
    y: usize,
}

impl<'a> Iterator for Fields<'a> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.x > self.bx.b.0 {
            self.x = self.bx.a.0;
            self.y += 1;
        }

        if self.y > self.bx.b.1 {
            return None;
        }

        let result = (self.x, self.y);
        self.x += 1;
        Some(result)
    }
}
