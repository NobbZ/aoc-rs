pub trait LightGrid {
    fn turn_on(&mut self, coord: (usize, usize));
    fn turn_off(&mut self, coord: (usize, usize));
    fn toggle(&mut self, coord: (usize, usize));
    fn brightness(&self) -> usize;
}
