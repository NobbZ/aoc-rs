mod instruction;
mod light_grid;
mod light_map;
mod light_set;
mod parser;
mod rectangle;

use self::{light_grid::LightGrid, light_map::LightMap, light_set::LightSet};
use crate::prelude::*;

const INPUT: &str = load_input!(2015 06);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), &mut LightSet::new()), 18055489783270121232},
        part2 => {run(Cursor::new(INPUT), &mut LightMap::new()), 12692723453761511877},
    ];
}

fn run<R, G>(r: R, grid: &mut G) -> Result<usize, Error>
where
    R: BufRead,
    G: LightGrid,
{
    use self::instruction::Instruction::*;

    let parsed = parser::parse(r);

    for instruction in parsed {
        match instruction {
            TurnOn(rect) => {
                for coord in rect.covered_fields() {
                    grid.turn_on(coord)
                }
            }
            TurnOff(rect) => {
                for coord in rect.covered_fields() {
                    grid.turn_off(coord)
                }
            }
            Toggle(rect) => {
                for coord in rect.covered_fields() {
                    grid.toggle(coord)
                }
            }
        }
    }

    Ok(grid.brightness())
}
