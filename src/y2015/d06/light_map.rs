use super::light_grid::LightGrid;

pub struct LightMap(Vec<u64>);

impl LightMap {
    pub fn new() -> Self {
        LightMap(vec![0; 1_000_000])
    }
}

impl LightGrid for LightMap {
    fn turn_on(&mut self, coord: (usize, usize)) {
        self.0[coord.0 * 1000 + coord.1] += 1;
    }

    fn turn_off(&mut self, coord: (usize, usize)) {
        let idx = coord.0 * 1000 + coord.1;
        if self.0[idx] > 0 {
            self.0[idx] -= 1;
        }
    }

    fn toggle(&mut self, coord: (usize, usize)) {
        self.0[coord.0 * 1000 + coord.1] += 2;
    }

    fn brightness(&self) -> usize {
        self.0.iter().sum::<u64>() as usize
    }
}
