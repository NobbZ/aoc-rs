use super::value::Value;

#[derive(Debug, Clone)]
pub enum Instruction {
    Not(Value),
    And(Value, Value),
    Or(Value, Value),
    RShift(Value, Value),
    LShift(Value, Value),
    Pure(Value),
}
