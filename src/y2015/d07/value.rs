#[derive(Debug, Clone)]
pub enum Value {
    V(u16),
    W(String),
}
