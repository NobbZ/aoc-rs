mod evaluator;
mod instruction;
mod parser;
mod token;
mod value;

use self::{instruction::Instruction, value::Value};
use crate::prelude::*;

const INPUT: &str = load_input!(2015 07);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 2389281469579493349},
        part2 => {run_2(Cursor::new(INPUT)), 13786727079565841174},
    ];
}

fn run_1<R>(r: R) -> Result<u16, Error>
where
    R: BufRead,
{
    let ctxt = parser::parse(r)?;

    Ok(evaluator::eval("a", &ctxt))
}

fn run_2<R>(r: R) -> Result<u16, Error>
where
    R: BufRead,
{
    let mut ctxt = parser::parse(r)?;

    let a = evaluator::eval("a", &ctxt);

    ctxt.insert("b".to_string(), Instruction::Pure(Value::V(a)));

    Ok(evaluator::eval("a", &ctxt))
}
