use std::collections::HashMap;

use super::{instruction::Instruction, value::Value};

pub fn eval(k: &str, ctxt: &HashMap<String, Instruction>) -> u16 {
    let mut cache: HashMap<String, u16> = HashMap::new();

    eval_do(k, ctxt, &mut cache)
}

fn eval_do(k: &str, ctxt: &HashMap<String, Instruction>, cache: &mut HashMap<String, u16>) -> u16 {
    use self::Instruction::*;

    if cache.contains_key(k) {
        return cache[k];
    }

    let v = match ctxt[k].clone() {
        Not(a) => !eval_arg(a, ctxt, cache),
        And(a, b) => eval_arg(a, ctxt, cache) & eval_arg(b, ctxt, cache),
        Or(a, b) => eval_arg(a, ctxt, cache) | eval_arg(b, ctxt, cache),
        RShift(a, b) => eval_arg(a, ctxt, cache) >> eval_arg(b, ctxt, cache),
        LShift(a, b) => eval_arg(a, ctxt, cache) << eval_arg(b, ctxt, cache),
        Pure(v) => eval_arg(v, ctxt, cache),
    };

    cache.insert(k.to_string(), v);

    v
}

fn eval_arg(
    v: Value,
    ctxt: &HashMap<String, Instruction>,
    cache: &mut HashMap<String, u16>,
) -> u16 {
    match v {
        Value::V(v) => v,
        Value::W(w) => eval_do(&w, ctxt, cache),
    }
}
