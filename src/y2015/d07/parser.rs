use std::collections::HashMap;

use super::{instruction::Instruction, token::Token};
use crate::prelude::*;

pub fn parse<R>(r: R) -> Result<HashMap<String, Instruction>, Error>
where
    R: BufRead,
{
    let mut wires = HashMap::new();
    let tokens = r.lines().map(|l| l.unwrap()).map(tokenize);

    for instr in tokens {
        use super::token::Token::*;

        match instr.as_slice() {
            [Not, arg, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::Not(arg.into()));
            }
            [arg1, And, arg2, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::And(arg1.into(), arg2.into()));
            }
            [arg1, Or, arg2, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::Or(arg1.into(), arg2.into()));
            }
            [arg1, RShift, arg2, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::RShift(arg1.into(), arg2.into()));
            }
            [arg1, LShift, arg2, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::LShift(arg1.into(), arg2.into()));
            }
            [val, Arrow, ID(id)] => {
                wires.insert(id.clone(), Instruction::Pure(val.into()));
            }
            _ => {
                return Err(format_err!("Unknown instruction set: {:?}", instr));
            }
        }
    }

    Ok(wires)
}

fn tokenize<S>(line: S) -> Vec<Token>
where
    S: AsRef<str> + std::fmt::Debug,
{
    let mut i = 0;
    let mut res: Vec<Token> = vec![];
    let chars = line.as_ref().chars().collect::<Vec<char>>();

    while i < chars.len() {
        i += match chars[i] {
            ' ' => 1,
            '-' => {
                res.push(Token::Arrow);
                2
            }
            c if c.is_ascii_lowercase() => tokenize_id(chars[i..].iter(), &mut res),
            c if c.is_ascii_uppercase() => tokenize_kw(chars[i..].iter(), &mut res),
            c if c.is_ascii_digit() => tokenize_val(chars[i..].iter(), &mut res),
            c => unimplemented!(
                "Char: {:?}, Remainder: {:?}, result: {:?}",
                c,
                chars[i..].iter().collect::<String>(),
                res,
            ),
        }
    }

    res
}

fn tokenize_id<'a>(chars: impl Iterator<Item = &'a char>, tokens: &mut Vec<Token>) -> usize {
    let token_val = chars
        .take_while(|c| c.is_ascii_lowercase())
        .collect::<String>();

    tokens.push(Token::ID(token_val.clone()));

    token_val.len()
}

fn tokenize_kw<'a>(chars: impl Iterator<Item = &'a char>, tokens: &mut Vec<Token>) -> usize {
    use self::Token::*;

    let token_val = chars
        .take_while(|c| c.is_ascii_uppercase())
        .collect::<String>();

    match token_val.as_ref() {
        "NOT" => tokens.push(Not),
        "AND" => tokens.push(And),
        "OR" => tokens.push(Or),
        "RSHIFT" => tokens.push(RShift),
        "LSHIFT" => tokens.push(LShift),
        _ => unreachable!("Invalid Command: {:?}; Tokens: {:?}", token_val, tokens),
    }

    token_val.len()
}

fn tokenize_val<'a>(chars: impl Iterator<Item = &'a char>, tokens: &mut Vec<Token>) -> usize {
    let token_str = chars.take_while(|c| c.is_ascii_digit()).collect::<String>();

    tokens.push(Token::Val(token_str.parse().unwrap()));

    token_str.len()
}
