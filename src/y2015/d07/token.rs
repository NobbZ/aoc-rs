use super::value::Value;

#[derive(Debug, Clone)]
pub enum Token {
    ID(String),
    Val(u16),
    Arrow,
    Not,
    And,
    Or,
    RShift,
    LShift,
}

impl From<&Token> for Value {
    fn from(token: &Token) -> Self {
        match token {
            Token::ID(s) => Value::W(s.to_string()),
            Token::Val(v) => Value::V(*v),
            _ => unimplemented!("{:?}", token),
        }
    }
}
