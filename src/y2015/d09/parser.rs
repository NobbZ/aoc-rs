use std::collections::HashMap;

use crate::prelude::*;

pub fn parse<R>(r: R) -> Result<HashMap<String, HashMap<String, usize>>, Error>
where
    R: BufRead,
{
    let mut hm = HashMap::new();

    for l in r.lines().map(|l| l.unwrap()) {
        let tokens = l.split(' ').collect::<Vec<_>>();

        let from = tokens[0];
        let to = tokens[2];
        let dist = tokens[4].parse::<usize>()?;

        let from_to = hm.entry(from.to_owned()).or_insert_with(HashMap::new);
        from_to.insert(to.to_owned(), dist);

        let to_from = hm.entry(to.to_owned()).or_insert_with(HashMap::new);
        to_from.insert(from.to_owned(), dist);
    }

    Ok(hm)
}
