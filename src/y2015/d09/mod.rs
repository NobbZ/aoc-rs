mod parser;

use std::cmp;

use permutohedron::Heap;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 09);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), std::usize::MAX, cmp::min), 15557503981608772456},
        part2 => {run(Cursor::new(INPUT), std::usize::MIN, cmp::max), 1398536204687975789},
    ];
}

fn run<R, Sel>(r: R, init: usize, sel: Sel) -> Result<usize, Error>
where
    R: BufRead,
    Sel: Fn(usize, usize) -> usize,
{
    let distances = parser::parse(r)?;

    let mut result = init;

    for route in Heap::new(&mut distances.keys().collect::<Vec<_>>()) {
        let mut length = 0;
        let mut last = route[0];

        for city in route[1..].iter() {
            length += distances[last][*city];
            last = city;
        }

        result = sel(result, length);
    }

    Ok(result)
}
