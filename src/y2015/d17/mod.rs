use std::collections::HashMap;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 17);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT), |r| Ok(r.values().sum())), 11094215064036527425},
        part2 => {run_1(Cursor::new(INPUT), minimum), 6185506036438099345},
    ];
}

fn run_1<R, F>(r: R, postprocessor: F) -> Result<usize, Error>
where
    R: BufRead,
    F: Fn(&HashMap<u64, usize>) -> Result<usize, Error>,
{
    let containers: Vec<usize> = r
        .lines()
        .map(|l| l.unwrap())
        .map(|l| l.parse().unwrap())
        .collect();

    let mut results: HashMap<u64, usize> = HashMap::new();
    solve_rec(&containers, 0, 150, 0, &mut results);

    postprocessor(&results)
}

fn minimum(r: &HashMap<u64, usize>) -> Result<usize, Error> {
    let min = match r.keys().min() {
        Some(v) => v,
        None => return Err(format_err!("No value where one was expected")),
    };

    Ok(r[min])
}

fn solve_rec(
    containers: &[usize],
    offset: usize,
    eggnog: usize,
    taken: u64,
    results: &mut HashMap<u64, usize>,
) {
    if offset == containers.len() {
        if eggnog == 0 {
            *results.entry(taken).or_insert(0) += 1;
        }
        return;
    }

    // we can either take or skip this container
    solve_rec(
        containers,
        offset + 1,
        eggnog - containers[offset],
        taken + 1,
        results,
    );
    solve_rec(containers, offset + 1, eggnog, taken, results);
}
