#[derive(Debug, PartialEq)]
pub struct Present {
    width: usize,
    height: usize,
    length: usize,
}

impl Present {
    pub fn new(w: usize, h: usize, l: usize) -> Self {
        Present {
            width: w,
            height: h,
            length: l,
        }
    }

    pub fn surface(&self) -> usize {
        2 * self.width * self.length + 2 * self.width * self.height + 2 * self.height * self.length
    }

    pub fn side_areas(&self) -> Vec<usize> {
        vec![
            self.width * self.length,
            self.width * self.height,
            self.height * self.length,
        ]
    }

    pub fn sides(&self) -> Vec<usize> {
        vec![self.width, self.height, self.length]
    }

    pub fn volume(&self) -> usize {
        self.width * self.height * self.length
    }
}
