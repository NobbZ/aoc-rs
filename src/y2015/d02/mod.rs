use std::io::BufRead;

use self::present::Present;
use crate::prelude::*;

const INPUT: &str = load_input!(2015 02);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), calc_paper), 8530579907645283593},
        part2 => {run(Cursor::new(INPUT), calc_ribbon), 1832361980063431433},
    ];
}

mod parser;
mod present;

fn run<R, F>(r: R, f: F) -> Result<usize, Error>
where
    R: BufRead,
    F: Fn(Present) -> Result<usize, Error>,
{
    parser::parse(r).map(f).sum()
}

#[allow(clippy::needless_pass_by_value)]
fn calc_paper(p: Present) -> Result<usize, Error> {
    Ok(p.surface() + p.side_areas().iter().min().unwrap())
}

#[allow(clippy::needless_pass_by_value)]
fn calc_ribbon(p: Present) -> Result<usize, Error> {
    let mut s = p.sides();
    s.sort();
    Ok(s.into_iter().take(2).map(|n| 2 * n).sum::<usize>() + p.volume())
}

macro_rules! test_description {
    ($name:ident : $fun:ident, { $a:expr, $b:expr, $c:expr } => $exp:expr) => {
        #[test]
        fn $name() {
            let p = Present::new($a, $b, $c);
            let actual = $fun(p);
            assert_eq!($exp, actual.unwrap());
        }
    };

    ($name:ident : $input:expr => $exp:expr) => {
        #[test]
        fn $name() {
            let parsed = parser::parse(Cursor::new($input)).collect::<Vec<_>>();
            assert_eq!($exp, parsed);
        }
    };
}

// Part 1 samples
test_description!(p2x3x4_requires_58_sqft:  calc_paper, {2, 3,  4} => 58);
test_description!(p1x1x10_requires_43_sqft: calc_paper, {1, 1, 10} => 43);

// Part 2 samples
test_description!(p2x3x4_requires_58_ft:  calc_ribbon, {2, 3,  4} => 34);
test_description!(p1x1x10_requires_43_ft: calc_ribbon, {1, 1, 10} => 14);

// Parser samples
test_description!(parse2x3x4: "2x3x4" => vec![Present::new(2, 3, 4)]);
test_description!(parse1x1x10: "1x1x10" => vec![Present::new(1, 1, 10)]);
test_description!(parse_two_lines: "2x3x4\n1x1x10" => vec![Present::new(2, 3, 4), Present::new(1, 1, 10)]);
