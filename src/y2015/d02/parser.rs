use super::present::Present;
// use crate::prelude::*;
use std::io::BufRead;

pub fn parse<R>(r: R) -> impl Iterator<Item = Present>
where
    R: BufRead,
{
    r.lines()
        .map(|l| {
            l.unwrap()
                .split('x')
                .map(|n| n.parse::<usize>().unwrap())
                .collect::<Vec<_>>()
        })
        .map(|present| {
            let present = &present;
            Present::new(present[0], present[1], present[2])
        })
}
