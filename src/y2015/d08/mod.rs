mod processor;

use std::cmp;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 08);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), processor::unescape), 4080133015542019800},
        part2 => {run(Cursor::new(INPUT), processor::escape), 174455967783580037},
    ];
}

fn run<R, F>(r: R, f: F) -> Result<usize, Error>
where
    R: BufRead,
    F: Fn(&str) -> String,
{
    let sums = r
        .lines()
        .map(|l| l.unwrap())
        .map(|x| (f(&x), x))
        .map(|(processed, orig)| (processed.len(), orig.len()))
        .fold((0, 0), |(p1, o1), (p2, o2)| (p1 + p2, o1 + o2));

    let greater = cmp::max(sums.0, sums.1);
    let lesser = cmp::min(sums.0, sums.1);

    Ok(greater - lesser)
}
