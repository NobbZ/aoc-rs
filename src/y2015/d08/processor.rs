pub fn unescape(s: &str) -> String {
    let chars = s.chars().collect::<Vec<_>>();

    let mut res = String::with_capacity(s.len());
    let mut l: Option<char> = None;
    let mut ll: Option<char> = None;
    let mut lll: Option<char> = None;

    for i in 0..chars.len() - 2 {
        let new = match (lll, ll, l, chars[i + 1]) {
            (Some('\\'), Some('x'), _, _) => {
                res.push(' '); // Use space as a dummy replacement
                (None, None, None)
            }
            (_, Some('\\'), Some('x'), c) => (Some('\\'), Some('x'), Some(c)),
            (_, _, Some('\\'), 'x') => (None, Some('\\'), Some('x')),
            (_, _, Some('\\'), '"') => {
                res.push('"');
                (None, None, None)
            }
            (_, _, Some('\\'), '\\') => {
                res.push('\\');
                (None, None, None)
            }
            (_, _, Some('\\'), c) => panic!("\\{:?}", c),
            (_, _, _, '\\') => (None, None, Some('\\')),
            (_, _, _, c) => {
                res.push(c);
                (None, None, None)
            }
        };

        lll = new.0;
        ll = new.1;
        l = new.2;
    }

    res
}

pub fn escape(s: &str) -> String {
    let mut res = String::with_capacity(s.len() + 5);

    res.push('"');

    for c in s.chars() {
        match c {
            '"' => res.push_str("\\\""),
            '\\' => res.push_str("\\\\"),
            _ => res.push(c),
        }
    }

    res.push('"');

    res
}
