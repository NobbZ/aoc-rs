#[derive(Debug, Clone)]
pub struct Password {
    pub chars: Vec<char>,
}

impl Password {
    pub fn new(s: &str) -> Password {
        Password {
            chars: s.chars().collect(),
        }
    }

    pub fn next(&self) -> Self {
        let mut res = self.chars.clone();

        for i in 0..res.len() {
            let idx = res.len() - i - 1;
            let carry = res[idx] == 'z';

            if carry {
                res[idx] = 'a';
            } else {
                res[idx] = (res[idx] as u8 + 1).into();
                break;
            }
        }

        Password { chars: res }
    }

    pub fn secure(&self) -> bool {
        let mut three = false;
        let mut invalids = false;
        let mut pairs = 0;

        let mut l: Option<char> = None;
        let mut ll: Option<char> = None;

        for c in self.chars.iter() {
            let c = *c;

            if let Some(l) = l {
                if let Some(ll) = ll {
                    three = three || ((c as u8 - 2) == ll as u8 && (c as u8 - 1) == l as u8)
                }
            }

            invalids = invalids || c == 'i' || c == 'o' || c == 'l';

            if Some(c) != ll && Some(c) == l {
                pairs += 1;
            }

            ll = l;
            l = Some(c);
        }

        three && !invalids && pairs >= 2
    }
}
