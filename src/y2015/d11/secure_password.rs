use super::password::Password;

pub struct SecurePasswords {
    current: Password,
}

impl SecurePasswords {
    pub fn new(p: Password) -> SecurePasswords {
        SecurePasswords { current: p }
    }
}

impl Iterator for SecurePasswords {
    type Item = Password;

    fn next(&mut self) -> Option<Self::Item> {
        self.current = self.current.next();

        while !self.current.secure() {
            self.current = self.current.next();
        }

        Some(self.current.clone())
    }
}
