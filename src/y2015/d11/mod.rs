mod password;
mod secure_password;

use self::{password::Password, secure_password::SecurePasswords};

const INPUT: &str = "hxbxwxba";

problem!{
    tests => [
        part1 => {run(INPUT, 1), 7727605502662672874},
        part2 => {run(INPUT, 2), 16721290577613101389},
    ];
}

fn run(input: &str, n: usize) -> Option<String> {
    let pw = SecurePasswords::new(Password::new(input)).nth(n - 1)?;

    Some(pw.chars.iter().collect())
}
