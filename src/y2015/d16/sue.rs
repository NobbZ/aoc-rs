use std::str::FromStr;

use crate::prelude::*;

const SUE: &str = "Sue ";
const CHILDREN: &str = "children: ";
const CATS: &str = "cats: ";
const SAMOYEDS: &str = "samoyeds: ";
const POMERANIANS: &str = "pomeranians: ";
const AKITAS: &str = "akitas: ";
const VIZSLAS: &str = "vizslas: ";
const GOLDFISH: &str = "goldfish: ";
const TREES: &str = "trees: ";
const CARS: &str = "cars: ";
const PERFUMES: &str = "perfumes: ";

#[derive(Debug)]
pub struct Sue {
    number: usize,
    children: Option<usize>,
    cats: Option<usize>,
    samoyeds: Option<usize>,
    pomeranians: Option<usize>,
    akitas: Option<usize>,
    vizslas: Option<usize>,
    goldfish: Option<usize>,
    trees: Option<usize>,
    cars: Option<usize>,
    perfumes: Option<usize>,
}

macro_rules! attr {
    ($id:ident; $getter:ident; $setter:ident; $resetter:ident) => {
        #[allow(unused)]
        pub fn $getter(&self) -> Option<usize> {
            self.$id
        }

        #[allow(unused)]
        pub fn $setter(&mut self, val: usize) {
            self.$id = Some(val);
        }

        #[allow(unused)]
        pub fn $resetter(&mut self) {
            self.$id = None;
        }
    };
}

macro_rules! check_attr {
    ($a:ident, $b:ident, $attr:ident) => {
        ($a.$attr.is_none() || $b.$attr.is_none() || $a.$attr == $b.$attr)
    };
}

macro_rules! check_attr_gt {
    ($a:ident, $b:ident, $attr:ident) => {
        ($a.$attr.is_none() || $b.$attr.is_none() || $a.$attr > $b.$attr)
    };
}

macro_rules! check_attr_lt {
    ($a:ident, $b:ident, $attr:ident) => {
        ($a.$attr.is_none() || $b.$attr.is_none() || $a.$attr < $b.$attr)
    };
}

impl Sue {
    pub fn new(number: usize) -> Self {
        Sue {
            number,
            children: None,
            cats: None,
            samoyeds: None,
            pomeranians: None,
            akitas: None,
            vizslas: None,
            goldfish: None,
            trees: None,
            cars: None,
            perfumes: None,
        }
    }

    pub fn get_number(&self) -> usize {
        self.number
    }

    attr!(children; get_children; set_children; reset_children);
    attr!(cats; get_cats; set_cats; reset_cats);
    attr!(samoyeds; get_samoyeds; set_samoyeds; reset_samoyeds);
    attr!(pomeranians; get_pomeranians; set_pomeranians; reset_pomeranians);
    attr!(akitas; get_akitas; set_akitas; reset_akitas);
    attr!(vizslas; get_vizslas; set_vizslas; reset_vizslas);
    attr!(goldfish; get_goldfish; set_goldfish; reset_goldfish);
    attr!(trees; get_trees; set_trees; reset_trees);
    attr!(cars; get_cars; set_cars; reset_cars);
    attr!(perfumes; get_perfumes; set_perfumes; reset_perfumes);

    pub fn matches(&self, o: &Self) -> bool {
        check_attr!(self, o, children)
            && check_attr!(self, o, cats)
            && check_attr!(self, o, samoyeds)
            && check_attr!(self, o, pomeranians)
            && check_attr!(self, o, akitas)
            && check_attr!(self, o, vizslas)
            && check_attr!(self, o, goldfish)
            && check_attr!(self, o, trees)
            && check_attr!(self, o, cars)
            && check_attr!(self, o, perfumes)
    }

    pub fn fuzzy(&self, o: &Self) -> bool {
        check_attr!(self, o, children)
            && check_attr!(self, o, samoyeds)
            && check_attr!(self, o, akitas)
            && check_attr!(self, o, vizslas)
            && check_attr!(self, o, cars)
            && check_attr!(self, o, perfumes)
            && check_attr_gt!(self, o, cats)
            && check_attr_gt!(self, o, trees)
            && check_attr_lt!(self, o, pomeranians)
            && check_attr_lt!(self, o, goldfish)
    }
}

impl FromStr for Sue {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut idx = 0;

        let mut sue = 0;
        let mut children = None;
        let mut cats = None;
        let mut samoyeds = None;
        let mut pomeranians = None;
        let mut akitas = None;
        let mut vizslas = None;
        let mut goldfish = None;
        let mut trees = None;
        let mut cars = None;
        let mut perfumes = None;

        while idx < s.len() {
            if s[idx..].starts_with(SUE) {
                idx += SUE.len();
                idx += read_usize(&s[idx..], &mut sue)? + 2;
            } else if s[idx..].starts_with(CHILDREN) {
                idx += CHILDREN.len();
                idx += read_option_usize(&s[idx..], &mut children)? + 2;
            } else if s[idx..].starts_with(CATS) {
                idx += CATS.len();
                idx += read_option_usize(&s[idx..], &mut cats)? + 2;
            } else if s[idx..].starts_with(SAMOYEDS) {
                idx += SAMOYEDS.len();
                idx += read_option_usize(&s[idx..], &mut samoyeds)? + 2;
            } else if s[idx..].starts_with(POMERANIANS) {
                idx += POMERANIANS.len();
                idx += read_option_usize(&s[idx..], &mut pomeranians)? + 2;
            } else if s[idx..].starts_with(AKITAS) {
                idx += AKITAS.len();
                idx += read_option_usize(&s[idx..], &mut akitas)? + 2;
            } else if s[idx..].starts_with(VIZSLAS) {
                idx += VIZSLAS.len();
                idx += read_option_usize(&s[idx..], &mut vizslas)? + 2;
            } else if s[idx..].starts_with(GOLDFISH) {
                idx += GOLDFISH.len();
                idx += read_option_usize(&s[idx..], &mut goldfish)? + 2;
            } else if s[idx..].starts_with(TREES) {
                idx += TREES.len();
                idx += read_option_usize(&s[idx..], &mut trees)? + 2;
            } else if s[idx..].starts_with(CARS) {
                idx += CARS.len();
                idx += read_option_usize(&s[idx..], &mut cars)? + 2;
            } else if s[idx..].starts_with(PERFUMES) {
                idx += PERFUMES.len();
                idx += read_option_usize(&s[idx..], &mut perfumes)? + 2;
            } else {
                return Err(format_err!(
                    "Unable to parse substring {:?} from {:?}.",
                    &s[idx..],
                    s
                ));
            }
        }

        Ok(Sue {
            number: sue,
            children,
            cats,
            samoyeds,
            pomeranians,
            akitas,
            vizslas,
            goldfish,
            trees,
            cars,
            perfumes,
        })
    }
}

fn read_usize(s: &str, dst: &mut usize) -> Result<usize, Error> {
    let mut i = 0;

    let bytes = s.as_bytes();

    while i < bytes.len() && (bytes[i] as char).is_ascii_digit() {
        i += 1;
    }

    if let Ok(n) = s[..i].parse() {
        *dst = n;
    } else {
        return Err(format_err!(
            "Unable to parrse {:?} as number. It was a substring of {:?}.",
            &s[..i],
            s
        ));
    }

    Ok(i)
}

fn read_option_usize(s: &str, dst: &mut Option<usize>) -> Result<usize, Error> {
    let mut n = 0;

    let i = read_usize(s, &mut n)?;

    *dst = Some(n);

    Ok(i)
}
