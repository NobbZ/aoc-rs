use super::sue::Sue;
use crate::prelude::*;

pub fn parse<R>(r: R) -> impl Iterator<Item = Sue>
where
    R: BufRead,
{
    r.lines().map(|l| l.unwrap()).map(|l| l.parse().unwrap())
}
