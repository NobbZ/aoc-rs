mod parser;
mod sue;

use self::sue::Sue;
use crate::prelude::*;

const INPUT: &str = load_input!(2015 16);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), exact), 10346499338674982396},
        part2 => {run(Cursor::new(INPUT), fuzzy), 14646838598150249928},
    ];
}

fn run<R, F>(r: R, selector: F) -> Result<usize, Error>
where
    R: BufRead,
    F: Fn(&Sue, &Sue) -> bool,
{
    let mut sue = Sue::new(0);

    sue.set_children(3);
    sue.set_cats(7);
    sue.set_samoyeds(2);
    sue.set_pomeranians(3);
    sue.set_akitas(0);
    sue.set_vizslas(0);
    sue.set_goldfish(5);
    sue.set_trees(3);
    sue.set_cars(2);
    sue.set_perfumes(1);

    match parser::parse(r).find(|s| selector(s, &sue)) {
        Some(s) => Ok(s.get_number()),
        None => Err(format_err!("No Sue found in input")),
    }
}

fn exact(this: &Sue, other: &Sue) -> bool {
    this.matches(other)
}

fn fuzzy(this: &Sue, other: &Sue) -> bool {
    this.fuzzy(other)
}
