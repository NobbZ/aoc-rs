use std::collections::HashMap;

use crate::prelude::*;

pub fn parse<R>(r: R) -> Result<HashMap<String, HashMap<String, isize>>, Error>
where
    R: BufRead,
{
    let mut hm = HashMap::new();

    for l in r.lines().map(|l| l.unwrap()) {
        match l.split(' ').collect::<Vec<_>>().as_slice() {
            [who, "would", "gain", amnt, "happiness", "units", "by", "sitting", "next", "to", whom] =>
            {
                let whom = whom[0..whom.len() - 1].to_string();

                let entry = hm.entry((*who).to_string()).or_insert_with(HashMap::new);
                entry.insert(whom, amnt.parse::<isize>()?);
            }
            [who, "would", "lose", amnt, "happiness", "units", "by", "sitting", "next", "to", whom] =>
            {
                let whom = whom[0..whom.len() - 1].to_string();

                let entry = hm.entry((*who).to_string()).or_insert_with(HashMap::new);
                entry.insert(whom, -amnt.parse::<isize>()?);
            }
            x => {
                println!("{:?}", x);
            }
        }
    }

    Ok(hm)
}
