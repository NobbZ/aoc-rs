mod parser;

use std::collections::HashMap;

use permutohedron::Heap;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 13);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT), 0), 3243138897542356980},
        part2 => {run(Cursor::new(INPUT), 1), 11488186652992817125},
    ];
}

fn run<R>(r: R, neutral_guests: usize) -> Result<isize, Error>
where
    R: BufRead,
{
    let mut map = parser::parse(r)?;

    let keys = map.keys().cloned().collect::<Vec<String>>();

    for name in keys.iter() {
        for i in 0..neutral_guests {
            let entry = map.entry(i.to_string()).or_insert_with(HashMap::new);
            entry.insert(name.to_string(), 0);
        }

        for i in 0..neutral_guests {
            let entry = map.entry(name.to_string()).or_default();
            entry.insert(i.to_string(), 0);
        }
    }

    match Heap::new(&mut map.keys().collect::<Vec<_>>())
        .map(|order| score(&order, &map))
        .max()
    {
        Some(result) => Ok(result),
        None => Err(format_err!("Empty heap!")),
    }
}

fn score<S>(order: &[S], relationships: &HashMap<String, HashMap<String, isize>>) -> isize
where
    S: AsRef<str> + std::fmt::Debug,
{
    let seats = order.len() as isize;

    let mut scores = Vec::with_capacity(seats as usize);

    for i in 0..seats {
        let left = order[(if i == 0 { seats } else { i } - 1) as usize].as_ref();
        let curr = order[i as usize].as_ref();
        let righ = order[((i + 1) % seats) as usize].as_ref();

        scores.push(relationships[curr][left] + relationships[curr][righ]);
    }

    scores.iter().sum()
}

#[cfg(test)]
const EXAMPLE: &str = r"Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.
";

#[test]
fn example_full() {
    let result = run(Cursor::new(EXAMPLE), 0).unwrap();

    assert_eq!(330, result);
}

#[test]
fn example_seats() {
    let map = parser::parse(Cursor::new(EXAMPLE)).unwrap();

    let order = vec!["David", "Alice", "Bob", "Carol"];

    let result = score(&order, &map);

    assert_eq!(330, result);
}

#[test]
fn example_seats_with_extra() {
    let mut map = parser::parse(Cursor::new(EXAMPLE)).unwrap();

    let keys = map.keys().cloned().collect::<Vec<String>>();

    for name in keys.iter() {
        for i in 0..1 {
            let entry = map.entry(i.to_string()).or_insert_with(HashMap::new);
            entry.insert(name.to_string(), 0);
        }

        for i in 0..1 {
            let entry = map.entry(name.to_string()).or_default();
            entry.insert(i.to_string(), 0);
        }
    }

    let order = vec!["0", "David", "Alice", "Bob", "Carol"];

    let result = score(&order, &map);

    assert_eq!(330 - 96, result);
}
