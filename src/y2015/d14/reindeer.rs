use std::{cmp, ops::AddAssign, str::FromStr};

use regex::Regex;

use crate::prelude::*;

#[derive(Debug, Default)]
pub struct Reindeer {
    speed: usize,
    fly_time: usize,
    pause_time: usize,

    pub time: usize,
    pub pos: usize,
    pub score: usize,
}

#[derive(Debug, Fail)]
#[fail(display = "Invalid reindeer")]
pub struct ReindeerParseError {}

impl Reindeer {
    pub fn fly<Int>(&self, duration: Int) -> usize
    where
        Int: Into<usize>,
    {
        let duration = duration.into();

        let sum_time = self.fly_time + self.pause_time;

        let full_flight = self.fly_time * self.speed;

        let full = duration / sum_time;
        let partial = duration % sum_time;

        full * full_flight + cmp::min(partial, self.fly_time) * self.speed
    }

    pub fn advance(&mut self) {
        let sum_time = self.fly_time + self.pause_time;
        let period = self.time % sum_time;

        if period < self.fly_time {
            self.pos += self.speed;
        }

        self.time += 1;
    }

    pub fn add_score<Int>(&mut self, n: Int)
    where
        usize: AddAssign<Int>,
    {
        self.score += n;
    }
}

impl FromStr for Reindeer {
    type Err = ReindeerParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(?P<name>[[:alpha:]]+) can fly (?P<speed>\d+) km/s for (?P<fly_time>\d+) seconds, but then must rest for (?P<pause_time>\d+) seconds.").unwrap();

        let caps = re.captures(s);

        match caps {
            Some(caps) => Ok(Reindeer {
                speed: caps.name("speed").unwrap().as_str().parse().unwrap(),
                fly_time: caps.name("fly_time").unwrap().as_str().parse().unwrap(),
                pause_time: caps.name("pause_time").unwrap().as_str().parse().unwrap(),
                ..Reindeer::default()
            }),
            None => Err(ReindeerParseError {}),
        }
    }
}
