mod parser;
mod reindeer;

use crate::prelude::*;

const INPUT: &str = load_input!(2015 14);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT), 2503), 8334910482350442242},
        part2 => {run_2(Cursor::new(INPUT), 2503), 16173518488310098141},
    ];
}

fn run_1<R>(r: R, duration: usize) -> Result<usize, Error>
where
    R: BufRead,
{
    match parser::parse(r).map(|r| r.fly(duration)).max() {
        Some(result) => Ok(result),
        None => Err(format_err!("No reindeers found")),
    }
}

fn run_2<R>(r: R, duration: usize) -> Result<usize, Error>
where
    R: BufRead,
{
    let mut reindeers: Vec<_> = parser::parse(r).collect();

    while reindeers[0].time <= duration {
        reindeers.iter_mut().for_each(|r| r.advance());

        reindeers.sort_by_key(|r| r.pos);
        reindeers.reverse();

        let best_pos = reindeers[0].pos;

        reindeers
            .iter_mut()
            .take_while(|r| r.pos == best_pos)
            .for_each(|r| r.add_score(1));
    }

    match reindeers.iter().map(|r| r.score).max() {
        Some(result) => Ok(result),
        None => Err(format_err!("No reindeers found")),
    }
}

#[cfg(test)]
const EXAMPLE: &str = r"Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
";

#[test]
fn mini_example() {
    let result = run_1(Cursor::new(EXAMPLE), 1000).unwrap();

    assert_eq!(1120, result);
}

#[test]
fn new_scoring_system() {
    let result = run_2(Cursor::new(EXAMPLE), 1000).unwrap();

    assert_eq!(689, result);
}
