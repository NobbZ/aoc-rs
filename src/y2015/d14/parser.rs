use super::reindeer::Reindeer;
use crate::prelude::*;

pub fn parse<R>(r: R) -> impl Iterator<Item = Reindeer>
where
    R: BufRead,
{
    r.lines()
        .map(|l| l.unwrap())
        .map(|l| l.parse::<Reindeer>().unwrap())
}
