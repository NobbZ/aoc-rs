mod look_and_say;

use self::look_and_say::LookAndSay;
use crate::prelude::*;

const INPUT: &str = "1321131112";

problem! {
    tests => [
        part1 => {run(INPUT, 40), 7651253089866252381},
        part2 => {run(INPUT, 50), 8222692476527362132},
    ];
}

fn run(input: &str, n: usize) -> Result<usize, Error> {
    match LookAndSay::new(input).take(n).last() {
        Some(chars) => Ok(chars.len()),
        None => Err(format_err!("No last element in iterator")),
    }
}
