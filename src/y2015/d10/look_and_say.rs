pub struct LookAndSay(Vec<char>);

impl LookAndSay {
    pub fn new(s: &str) -> Self {
        LookAndSay(s.chars().collect())
    }
}

impl Iterator for LookAndSay {
    type Item = Vec<char>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut new = Vec::with_capacity(2 * self.0.len());

        let mut i = 0;

        while i < self.0.len() {
            let cnt = count(&self.0[i..]);

            for c in cnt.to_string().chars() {
                new.push(c);
            }
            new.push(self.0[i]);

            i += cnt;
        }

        self.0 = new.clone();

        Some(new)
    }
}

fn count(chars: &[char]) -> usize {
    let first = chars[0];

    chars.iter().take_while(|e| **e == first).count()
}
