mod invalid_char_error;
mod none_error;

pub use self::{
    invalid_char_error::InvalidCharError,
    none_error::{option_to_result, NoneError},
};
