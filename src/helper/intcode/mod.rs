#[derive(Clone)]
pub struct Computer {
    memory: Vec<i128>,
    pc: usize,
}

impl Computer {
    pub fn new(init_mem: Vec<i128>) -> Self {
        Computer {
            memory: init_mem,
            pc: 0,
        }
    }

    pub fn poke(&mut self, addr: usize, val: i128) {
        self.memory[addr] = val;
    }

    pub fn peek(&self, addr: usize) -> i128 {
        self.memory[addr]
    }

    pub fn exec(&mut self) {
        let (opcode, modes) = parse_intcode(self.peek(self.pc));

        match opcode {
            Opcode::Add => self.apply(|a, b| a + b, modes),
            Opcode::Mul => self.apply(|a, b| a * b, modes),
            Opcode::Stop => return,
        }

        self.exec()
    }

    fn apply<Op>(&mut self, f: Op, _modes: Vec<()>)
    where
        Op: Fn(i128, i128) -> i128,
    {
        let a = self.peek(self.peek(self.pc + 1) as usize);
        let b = self.peek(self.peek(self.pc + 2) as usize);

        let r = f(a, b);

        self.poke(self.peek(self.pc + 3) as usize, r);

        self.pc += 4; // Command, 2 Args, Result
    }
}

fn parse_intcode(intcode: i128) -> (Opcode, Vec<()>) {
    let (opcode, _arity) = to_opcode(intcode % 100);
    (opcode, vec![])
}

enum Opcode {
    Add,
    Mul,
    Stop,
}

fn to_opcode(n: i128) -> (Opcode, u8) {
    use Opcode::*;
    match n {
        1 => (Add, 3),
        2 => (Mul, 3),
        99 => (Stop, 0),
        _ => unreachable!(),
    }
}
