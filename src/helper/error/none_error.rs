use failure_derive::Fail;

#[derive(Debug, Fail)]
#[fail(display = "Expected Some(value) but got None")]
pub struct NoneError;

pub fn option_to_result<T>(o: Option<T>) -> Result<T, NoneError> {
    match o {
        Some(v) => Ok(v),
        None => Err(NoneError {}),
    }
}
