use failure_derive::Fail;

#[derive(Debug, Fail)]
#[fail(display = "Expected {:?}, got {:?}.", expected, seen)]
pub struct InvalidCharError {
    seen: char,
    expected: Vec<char>,
}

impl InvalidCharError {
    // pub fn new<C>(seen: C, expected: Vec<C>) -> Self
    // where
    //     C: Into<char>,
    // {
    //     InvalidCharError {
    //         seen: seen.into(),
    //         expected: expected.into_iter().map(|c| c.into()).collect(),
    //     }
    // }
}
