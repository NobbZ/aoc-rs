use std::{
    collections::{hash_map::RandomState, HashSet},
    convert::TryFrom,
    iter::FromIterator,
};

use crate::prelude::*;

const INPUT: &str = load_input!(2019 03);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 6275174295707589086},
        part2 => {run_2(Cursor::new(INPUT)), 7732708988214858184},
    ];
}

fn run_1<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let wires: Vec<HashSet<(i128, i128), RandomState>> = r
        .lines()
        .map(|l| l.unwrap())
        .map(parse_line)
        .map(connect)
        .map(HashSet::from_iter)
        .collect();

    let (wire1, wire2) = (wires[0].clone(), wires[1].clone());

    let result = wire1
        .intersection(&wire2)
        .map(manhattan_distance)
        .filter(|&dist| dist != 0)
        .min()
        .unwrap(); // We can assume that there is always a solution to the problem

    Ok(result)
}

fn run_2<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let wires: Vec<Vec<(i128, i128)>> = r
        .lines()
        .map(|l| l.unwrap())
        .map(parse_line)
        .map(connect)
        .collect();

    let (wire1, wire2) = (wires[0].clone(), wires[1].clone());

    let coords1 = wire_to_set(&wire1);
    let coords2 = wire_to_set(&wire2);

    let intersections: HashSet<_> = coords1.intersection(&coords2).cloned().collect();

    let result = intersections
        .into_iter()
        .map(get_wire_length(wire1, wire2))
        .min()
        .unwrap();

    Ok(result)
}

fn parse_line<Str>(w: Str) -> Vec<RelativeSection>
where
    Str: AsRef<str>,
{
    w.as_ref()
        .split(',')
        .map(RelativeSection::try_from)
        .map(|rs| rs.unwrap())
        .collect()
}

fn manhattan_distance(coords: &(i128, i128)) -> i128 {
    coords.0.abs() + coords.1.abs()
}

fn get_wire_length<Coords>(wire1: Coords, wire2: Coords) -> impl FnMut((i128, i128)) -> i128
where
    Coords: AsRef<[(i128, i128)]>,
{
    move |coord| {
        let cnt1 = wire1.as_ref().iter().position(|c| c == &coord).unwrap();
        let cnt2 = wire2.as_ref().iter().position(|c| c == &coord).unwrap();
        (cnt1 + cnt2) as i128
    }
}

fn wire_to_set<Coords>(coords: Coords) -> HashSet<(i128, i128)>
where
    Coords: AsRef<[(i128, i128)]>,
{
    let mut set: HashSet<_> = coords.as_ref().iter().cloned().collect();
    set.remove(&(0, 0));
    set
}

fn connect(input: Vec<RelativeSection>) -> Vec<(i128, i128)> {
    let mut result = vec![(0, 0)];

    for rs in input.iter() {
        let last_pos = *result.last().unwrap();
        let mut positions = (1..=rs.distance)
            .map(|distance| steps(last_pos, rs.direction, distance))
            .collect::<Vec<_>>();

        result.append(&mut positions);
    }

    result
}

fn steps(pos: (i128, i128), direction: Direction, distance: i128) -> (i128, i128) {
    let (x, y) = pos;

    match direction {
        Direction::Up => (x, y + distance),
        Direction::Down => (x, y - distance),
        Direction::Left => (x - distance, y),
        Direction::Right => (x + distance, y),
    }
}

#[derive(Debug, Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone, Copy)]
#[allow(dead_code)]
struct RelativeSection {
    direction: Direction,
    distance: i128,
}

impl TryFrom<&str> for RelativeSection {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let b = s.as_bytes();

        let distance = std::str::from_utf8(&b[1..])?.parse()?;

        match b[0] as char {
            'U' => Ok(Self {
                distance,
                direction: Direction::Up,
            }),
            'D' => Ok(Self {
                distance,
                direction: Direction::Down,
            }),
            'L' => Ok(Self {
                distance,
                direction: Direction::Left,
            }),
            'R' => Ok(Self {
                distance,
                direction: Direction::Right,
            }),
            _ => unreachable!(), // todo: make return proper error
        }
    }
}

macro_rules! test_description {
    ($name:ident : run_1, $input:expr => $exp:expr) => {
        #[test]
        fn $name() {
            let cursor = Cursor::new($input);
            assert_eq!($exp, run_1(cursor).unwrap());
        }
    };

    ($name:ident : run_2, $input:expr => $exp:expr) => {
        #[test]
        fn $name() {
            let cursor = Cursor::new($input);
            assert_eq!($exp, run_2(cursor).unwrap());
        }
    };
}

// Part 1
test_description!(part1_should_be_6: run_1, "R8,U5,L5,D3\nU7,R6,D4,L4" => 6);
test_description!(part1_should_be_159: run_1, "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83" => 159);
test_description!(part1_should_be_135: run_1, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7" => 135);

// Part 2
test_description!(part2_example_30: run_2, "R8,U5,L5,D3\nU7,R6,D4,L4" => 30);
test_description!(part2_example_610: run_2, "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83" => 610);
test_description!(part2_example_410: run_2, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7" => 410);
