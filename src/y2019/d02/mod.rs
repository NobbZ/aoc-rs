use crate::{helper::intcode::Computer, prelude::*};

const INPUT: &str = load_input!(2019 02);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 17648715700255952111},
        part2 => {run_2(Cursor::new(INPUT)), 16787242910384161994},
    ];
}

fn run_1<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let v = r
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            l.split(',')
                .map(|n| n.parse::<i128>().unwrap())
                .collect::<Vec<_>>()
        })
        .next()
        .unwrap();

    let mut vm = Computer::new(v);

    vm.poke(1, 12);
    vm.poke(2, 2);

    vm.exec();

    Ok(vm.peek(0))
}

fn run_2<R>(r: R) -> Result<i128, Error>
where
    R: BufRead,
{
    let v = r
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            l.split(',')
                .map(|n| n.parse::<i128>().unwrap())
                .collect::<Vec<_>>()
        })
        .next()
        .unwrap();

    let vm = Computer::new(v);

    let nouns = 0..=99;
    let verbs = 0..=99;

    match nouns
        .flat_map(|n: i128| verbs.clone().map(move |v: i128| (n, v)))
        .map(|(n, v)| {
            let mut vm = vm.clone();
            vm.poke(1, n);
            vm.poke(2, v);
            vm.exec();
            (n, v, vm.peek(0))
        })
        .find(|(_, _, n)| *n == 19_690_720)
    {
        Some((n, v, _)) => Ok(n * 100 + v),
        None => unreachable!(),
    }
}
