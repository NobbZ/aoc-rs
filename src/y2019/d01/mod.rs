use crate::prelude::*;

const INPUT: &str = load_input!(2019 01);

problem! {
    tests => [
        part1 => {run_1(Cursor::new(INPUT)), 17707609619618910836},
        part2 => {run_2(Cursor::new(INPUT)), 3401709936948060751},
    ];
}

fn run_1<R>(r: R) -> Result<u128, Error>
where
    R: BufRead,
{
    Ok(r.lines()
        .map(|l| l.unwrap())
        .map(|mass_str: String| mass_str.parse::<u128>().unwrap())
        .map(fuel_for_mass)
        .sum())
}

fn run_2<R>(r: R) -> Result<u128, Error>
where
    R: BufRead,
{
    Ok(r.lines()
        .map(|l| l.unwrap())
        .map(|mass| mass.parse::<u128>().unwrap())
        .map(fuel_for_mass)
        .map(add_fuel)
        .sum())
}

fn fuel_for_mass(mass: u128) -> u128 {
    mass / 3 - 2
}

fn checked_fuel_for_mass(mass: u128) -> Option<u128> {
    (mass / 3).checked_sub(2)
}

fn add_fuel(mass: u128) -> u128 {
    add_fuel_rec(mass, mass)
}

fn add_fuel_rec(mass: u128, sum: u128) -> u128 {
    match checked_fuel_for_mass(mass) {
        None => sum,
        Some(mass) => add_fuel_rec(mass, sum + mass),
    }
}

macro_rules! test_description {
    ($name:ident : $fun:ident, $arg:expr => $exp:expr) => {
        #[test]
        fn $name() {
            assert_eq!($exp, $fun($arg));
        }
    };
}

test_description!(fuel_for_12_mass:     fuel_for_mass,      12 =>      2);
test_description!(fuel_for_14_mass:     fuel_for_mass,      14 =>      2);
test_description!(fuel_for_1969_mass:   fuel_for_mass,   1_969 =>    654);
test_description!(fuel_for_100756_mass: fuel_for_mass, 100_756 => 33_583);

test_description!(fuel_for_2_fuel:     add_fuel,      2 =>      2);
test_description!(fuel_for_654_fuel:   add_fuel,    654 =>    966);
test_description!(fuel_for_33583_fuel: add_fuel, 33_583 => 50_346);
