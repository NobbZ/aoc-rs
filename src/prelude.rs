pub use std::io::{BufRead, Cursor, Read};

pub use failure::{format_err, Error};
pub use failure_derive::Fail;
