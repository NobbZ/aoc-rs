use crate::prelude::*;

const INPUT: &str = load_input!(2016 01);

problem! {
    tests => [
        part1 => {run(Cursor::new(INPUT)), 10263667190582992611},
    ];
}

fn run<R>(r: R) -> Result<isize, Error>
where
    R: BufRead,
{
    let mut dir: u8 = 0;
    let mut pos: (isize, isize) = (0, 0);

    r.split(b',')
        .map(|i| String::from_utf8(i.unwrap()).unwrap())
        .map(|s| s.trim().to_owned())
        .map(|s| (s[0..1].to_owned(), s[1..].to_owned()))
        .map(|(d, s)| (d, s.parse::<isize>().unwrap()))
        .for_each(|(d, s)| {
            if d == "R" {
                dir = (dir + 1) % 4
            } else if dir == 0 {
                dir = 3
            } else {
                dir -= 1
            };

            match dir {
                0 => pos = (pos.0 + s, pos.1),
                1 => pos = (pos.0, pos.1 + s),
                2 => pos = (pos.0 - s, pos.1),
                3 => pos = (pos.0, pos.1 - s),
                _ => unreachable!("direction is {:?}", dir),
            };
        });

    Ok(pos.0.abs() + pos.1.abs())
}
