# Advent of Code - RUST

This are my solutions to the [advent of code project](https://adventofcode.com/).

## Run

### No spoilers

This will simply run all solutions and print the hashed result.

```
$ cargo run --release
```

### Spoilers

This will also print out the actual solution to the problem.

```
$ cargo run --release -- --spoil
```

