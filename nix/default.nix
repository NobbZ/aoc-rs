{sources ? import ./sources.nix {}}: let
  niv = self: super: {inherit sources;};
  rust = import "${sources.nixpkgs-mozilla.outPath}/rust-overlay.nix";
in
  import sources.nixpkgs {
    overlays = [niv rust];
    config = {};
  }
