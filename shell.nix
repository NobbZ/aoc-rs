let
  pkgs = import ./nix {};
  rust = pkgs.latest.rustChannels.nightly;
in
  pkgs.mkShell {nativeBuildInputs = [rust.rust rust.cargo];}
